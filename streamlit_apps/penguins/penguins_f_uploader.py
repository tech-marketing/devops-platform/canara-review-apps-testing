import matplotlib.pyplot as plt
import streamlit as st
import pandas as pd
import seaborn as sns

st.title("Penguins Dataset")
st.markdown("Create Scatterplots about penguins")

selected_species = st.selectbox("What species do you want to visualize?", ['Adelie', 'Gentoo', 'Chinstrap'])

# Plot features
markers = {"Adelie": "X", "Gentoo": "s", "Chinstrap":'o'}
sns.set_style('darkgrid')

# File Uploader

penguin_file = st.file_uploader("Upload your own dataset")

if penguin_file is not None:
    penguins_df = pd.read_csv(penguin_file)

    # Variables selection
    select_x_vr = st.selectbox("Select x variable for the plot",
                               ['bill_length_mm', 'bill_depth_mm', 'flipper_length_mm', 'body_mass_g'])
    select_y_vr = st.selectbox("Select y variable for the plot",
                               ['bill_length_mm', 'bill_depth_mm', 'flipper_length_mm', 'body_mass_g'])

    select_gender = st.selectbox("Select penguin Gender", ['all penguins', 'male penguins', 'female penguins'])

    if select_gender == 'male penguins':
        penguins_df = penguins_df[penguins_df['sex'] == 'male']

    elif select_gender == 'female penguins':
        penguins_df = penguins_df[penguins_df['sex'] == 'female']

    else:
        pass

    fig, ax = plt.subplots()
    ax = sns.scatterplot(data=penguins_df, x=penguins_df[select_x_vr],
                         y=penguins_df[select_y_vr], hue='species', markers=markers, style='species')
    plt.xlabel("Selected X variable")
    plt.ylabel("Selected Y variable")
    plt.title(f"Scatterplot of {selected_species}")
    st.pyplot(fig)

else:
    st.stop()

