import matplotlib.pyplot as plt
import streamlit as st
import numpy as np

bn_dist = np.random.binomial(1, .5, 100) #fliping a coin 1 time and tested 100 time
list_means = []

for i in range(0, 10000):
    list_means.append(np.random.choice(bn_dist, 100, replace=True).mean())

fig, ax = plt.subplots()
ax = plt.hist(list_means)

st.pyplot(fig)

st.text_input(label='text')